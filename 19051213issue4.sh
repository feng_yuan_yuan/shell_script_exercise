if type -p java; then
echo 'Java环境已经被安装.'
exit 0
else
#没有检测成功则安装java环境
echo '开始安装java环境...'
Javahome=/opt/software/jdk8
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u141-b15/336fa29ff2bb4ef291e347e091f7f4a7/jdk-8u141-linux-x64.tar.gz"
mkdir -p ${Javahome}
tar -xvf jdk-8u141-linux-x64.tar.gz -C ${java_home} --strip-components 1
echo "安装通道$PATH:${Javahome}/bin" >> /etc/profile
echo '设置安装通道成功.'
source /etc/profile
echo '安装成功'
fi
